/*
Decryption Test
	
Copyright (c) 2015, Logan Matthew Nichols

Permission to use, copy, modify, and/or distribute this software for any 
purpose with or without fee is hereby granted, provided that the above 
copyright notice and this permission notice appear in all copies.

THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES WITH 
REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF MERCHANTABILITY AND 
FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY SPECIAL, DIRECT, 
INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM 
LOSS OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR 
OTHER TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE OR 
PERFORMANCE OF THIS SOFTWARE.
*/

#include <iostream>
#include <fstream>
#include <string>
#include <vector>

using namespace std;

int main() {

	int shift;
	vector<string> raw;
	vector<string> output;
	vector<int> shifts;
	char *tempChar;
	string tempString;
	ofstream fileOut;
	ifstream fileIn;
	string passwd;
	cout << "Enter password: ";
	getline(cin, passwd);
	char *pw = (char*)passwd.c_str();
	int pw_counter = 0;
	string path;
	
	cout << "File path: ";
	getline(cin, path);
	
	fileIn.open((path + ".key").c_str());
	if (fileIn.fail()) {
		cout << "Unable to open key file" << endl;
		cin.get();
		return 0;
	} else {
		while (fileIn >> shift) {
			shift = (shift / pw[pw_counter]) - pw[pw_counter];
			shifts.push_back(shift);
			if (pw_counter < sizeof(pw)) {
				pw_counter++;
			} else {
				pw_counter = 0;
			}
		}
	}
	fileIn.close();
	
	fileIn.open(path.c_str());
	string next;
	if (fileIn.fail()) {
		cout << "Unable to open output file" << endl;
		cin.get();
		return 0;
	} else {
		while (getline(fileIn, next)) {
			raw.push_back(next);
		}
	}
	fileIn.close();
	
	/*for (int i = 0; i < (int)raw.size(); i++) { // output initial values
		cout << "\t" << i + 1 << ":" << raw[i] << endl;
	}*/
	
	bool pass = true;
	int place = 0;
	int tempPlace = 0;
	
	for (int i = 0; i < (int)raw.size(); i++) {
		tempChar = (char*)raw[i].c_str();
		tempString = "";
		for (int j = 0; j < raw[i].length(); j++) {
			tempChar[j] -= shifts[j + place];
			tempString += tempChar[j];
			tempPlace++;
		}
		place += tempPlace;
		tempPlace = 0;
		output.push_back(tempString);
	}
	
	fileOut.open("decoded.txt");
	if (fileOut.fail()) {
		cout << "Unable to write to decoded file" << endl;
		cin.get();
		return 0;
	} else {
		for (int i = 0; i < (int)raw.size(); i++) {
			fileOut << output[i] << endl;
		}
	}
	fileOut.close();
	
	cout << "Decode complete" << endl << "Press [ENTER] to close...";
	cin.get();
	
	return 0;
	
	cout << endl << "Press [ENTER] to close...";
	cin.get();
	
	return 0;
	
}
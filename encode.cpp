/*
Encryption Test
	
Copyright (c) 2015, Logan Matthew Nichols

Permission to use, copy, modify, and/or distribute this software for any 
purpose with or without fee is hereby granted, provided that the above 
copyright notice and this permission notice appear in all copies.

THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES WITH 
REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF MERCHANTABILITY AND 
FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY SPECIAL, DIRECT, 
INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM 
LOSS OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR 
OTHER TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE OR 
PERFORMANCE OF THIS SOFTWARE.
*/

#include <iostream>
#include <fstream>
#include <string>
#include <vector>
#include <time.h>

using namespace std;

int main() {
	
	int shift;
	vector<string> raw;
	vector<string> output;
	vector<int> shifts;
	//vector<char> shifts; 
	srand(time(NULL));
	char *tempChar;
	string tempString;
	ofstream fileOut;
	ifstream fileIn;
	string path;
	
	cout << "File path: ";
	getline(cin, path);
	
	fileIn.open(path.c_str());
	string next;
	if (fileIn.fail()) {
		cout << endl << "Unable to open input file" << endl;
		cin.get();
		return 0;
	} else {
		while (getline(fileIn, next)) {
			raw.push_back(next);
		}
	}
	fileIn.close();
	
	/*for (int i = 0; i < (int)raw.size(); i++) { // output initial values
		cout << "\t" << i + 1 << ":" << raw[i] << endl;
	}*/
	
	//cout << "Output:" <bn  < endl;
	bool pass = true;
	
	for (int i = 0; i < (int)raw.size(); i++) { // encrypt values
		tempChar = (char*)raw[i].c_str();
		tempString = "";
		for (int j = 0; j < raw[i].length(); j++) {
			do {
				//shift = 25 - rand() % 50;
				shift = 5 - rand() % 10;
				//shift = rand() % 50;
				pass = 
					shift + tempChar[j] <= 255 
					&& 
					shift + tempChar[j] >= 0
				;
			} while (!pass);
			tempChar[j] += shift;
			tempString += tempChar[j];
			shifts.push_back(shift);
		}
		output.push_back(tempString);
	}
	
	string passwd;
	cout << "Enter password: ";
	getline(cin, passwd);
	char *pw = (char*)passwd.c_str();
	int pw_counter = 0;
	
	fileOut.open((path + ".key").c_str());
	if (fileOut.fail()) {
		cout << "unable to write to key file" << endl;
		cin.get();
		return 0;
	} else {
		for (int i = 0; i < (int)shifts.size(); i++) {
			fileOut << (shifts[i] + pw[pw_counter]) * pw[pw_counter] << endl;
			if (pw_counter < sizeof(pw)) {
				pw_counter++;
			} else {
				pw_counter = 0;
			}
		}
	}
	fileOut.close();
	
	fileOut.open("encoded.txt");
	if (fileOut.fail()) {
		cout << "unable to write to output file" << endl;
		cin.get();
		return 0;
	} else {
		for (int i = 0; i < (int)raw.size(); i++) {
			fileOut << output[i] << endl;
		}
	}
	fileOut.close();
	
	cout << "Encode complete" << endl << "Press [ENTER] to close...";
	cin.get();
	
	return 0;
	
}